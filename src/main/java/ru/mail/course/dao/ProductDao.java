package ru.mail.course.dao;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.dto.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao {
    @NotNull List<Product> getAll() throws SQLException;

    @NotNull List<Product> getByManufacturer(@NotNull String manufacturer) throws SQLException;

    void create(@NotNull Product product) throws SQLException;

    @NotNull List<Product> getByName(@NotNull String name) throws SQLException;

    int deleteByName(@NotNull String productName) throws SQLException;
}
