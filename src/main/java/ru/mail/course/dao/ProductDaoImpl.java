package ru.mail.course.dao;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.course.DBConnectionData;
import ru.mail.course.dto.Product;

import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с БД для сущности Товар ({@link Product}).
 */
@NoArgsConstructor
public final class ProductDaoImpl implements ProductDao {
    @SuppressWarnings("NullableProblems")
    @NotNull
    private Connection connection;

    @Inject
    public ProductDaoImpl(@NotNull DBConnectionData connectionData) {
        try {
            this.connection = DriverManager.getConnection(connectionData.getUrl(), connectionData.getUser(), connectionData.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public List<Product> getAll() throws SQLException {
        String query = "SELECT * FROM Product";
        ResultSet resultSet;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            resultSet = statement.executeQuery();
            return getFromResultSet(resultSet);
        }
    }

    @NotNull
    @Override
    public List<Product> getByManufacturer(@NotNull String manufacturer) throws SQLException {
        String query = "SELECT * FROM Product WHERE manufacturer = ?";
        ResultSet resultSet;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, manufacturer.trim());
            resultSet = statement.executeQuery();
            return getFromResultSet(resultSet);
        }
    }

    @Override
    public void create(@NotNull Product product) throws SQLException {
        String id;
        if (product.getId() != null) {
            id = product.getId().toString();
        } else {
            id = "nextval('product_id_seq')";
        }
        String query = "INSERT INTO Product (id, name, manufacturer, count) VALUES(" + id + ", ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, product.getName());
            statement.setString(2, product.getManufacturer());
            statement.setInt(3, product.getCount());
            statement.executeUpdate();
        }
    }

    @Override
    public @NotNull List<Product> getByName(@NotNull String productName) throws SQLException {
        String query = "SELECT * FROM Product WHERE name = ?";
        ResultSet resultSet;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, productName);
            resultSet = statement.executeQuery();
            return getFromResultSet(resultSet);
        }
    }

    @Override
    public int deleteByName(@NotNull String productName) throws SQLException {
        String query = "DELETE FROM Product WHERE name = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, productName);
            return statement.executeUpdate();
        }
    }

    @NotNull
    private List<Product> getFromResultSet(@NotNull ResultSet resultSet) throws SQLException {
        List<Product> products = new ArrayList<>();
        while(resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String manufacturer = resultSet.getString("manufacturer");
            int count = resultSet.getInt("count");
            products.add(new Product(id, name, manufacturer, count));
        }
        return products;
    }
}
