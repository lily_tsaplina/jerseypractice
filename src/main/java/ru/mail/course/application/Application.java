package ru.mail.course.application;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.JDBCLoginService;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.SecurityHandlerBuilder;
import ru.mail.course.filter.OnlyOnePostRequestFilter;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Точка входа приложения.
 */
public final class Application {
    /**
     * Точка входа приложения.
     *
     * @param args аргументы из команды запуска приложения
     * @throws Exception ошибка
     */
    public static void main(@NotNull String[] args) throws Exception {
        final Server server = new Server();
        final HttpConfiguration httpConfig = new HttpConfiguration();
        final HttpConnectionFactory httpConnectionFactory = new HttpConnectionFactory(httpConfig);
        final ServerConnector serverConnector = new ServerConnector(server, httpConnectionFactory);
        serverConnector.setHost("localhost");
        serverConnector.setPort(3466);
        server.setConnectors(new Connector[]{serverConnector});

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.addServlet(HttpServletDispatcher.class, "/*");
        context.addEventListener(new GuiceListener());

        final OnlyOnePostRequestFilter filter;
        filter = new OnlyOnePostRequestFilter();
        final FilterHolder filterHolder = new FilterHolder(filter);
        context.addFilter(filterHolder, "/*", EnumSet.of(DispatcherType.REQUEST));

        final String jdbc_config = Application.class.getResource("/jdbc_config").toExternalForm();
        final JDBCLoginService jdbcLoginService = new JDBCLoginService("login", jdbc_config);
        final ConstraintSecurityHandler securityHandler = new SecurityHandlerBuilder().build(server, jdbcLoginService);
        securityHandler.setHandler(context);

        server.setHandler(securityHandler);

        server.start();
    }
}
