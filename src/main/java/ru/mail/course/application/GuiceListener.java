package ru.mail.course.application;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Scopes;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.DBConnectionData;
import ru.mail.course.controller.ProductController;
import ru.mail.course.controller.StaticResourceController;
import ru.mail.course.dao.ProductDao;
import ru.mail.course.dao.ProductDaoImpl;

import javax.servlet.ServletContext;
import java.util.Collections;
import java.util.List;

/**
 * Конфигурация биндингов для Guice.
 */
public final class GuiceListener extends GuiceResteasyBootstrapServletContextListener {
    @Override
    protected @NotNull List<? extends Module> getModules(@NotNull ServletContext context) {
        return Collections.singletonList(new GuiceModule());
    }

    private static final class GuiceModule extends AbstractModule {
        @SuppressWarnings("PointlessBinding")
        @Override
        protected void configure() {
            bind(ProductController.class);
            bind(StaticResourceController.class);
            bind(DBConnectionData.class).toInstance(new DBConnectionData(
                            "jdbc:postgresql://127.0.0.1:5432/products",
                            "postgres",
                            "blossom"
                    )
            );
            bind(ProductDao.class).to(ProductDaoImpl.class).in(Scopes.SINGLETON);
        }
    }
}
