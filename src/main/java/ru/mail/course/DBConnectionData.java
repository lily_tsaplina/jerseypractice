package ru.mail.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public final class DBConnectionData {
    @NotNull
    String url;
    @NotNull
    String user;
    @NotNull
    String password;
}
