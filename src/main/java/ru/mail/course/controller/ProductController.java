package ru.mail.course.controller;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.dao.ProductDao;
import ru.mail.course.dto.Product;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

/**
 * Контроллер для обработки запросов, связанных с сущностью Товар ({@link Product}).
 */
@Path("/product")
public final class ProductController {
    @NotNull
    private ProductDao productDao;

    @Inject
    public ProductController(@NotNull ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * Получить все товары.
     *
     * @return http-ответ со списком всех товаров
     * @throws SQLException ошибка выполнения sql
     */
    @NotNull
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() throws SQLException {
        List<Product> products = productDao.getAll();
        return Response.ok(products).build();
    }

    /**
     * Создать новый товар.
     *
     * @param product товар для сохранения
     * @throws SQLException ошибка выполнения sql
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(@NotNull Product product) throws SQLException {
        productDao.create(product);
    }

    /**
     * Удалить товары с заданным именем.
     *
     * @param name название товара
     * @return http-ответ с кодом 200, если были удалены товары, иначе 400
     * @throws SQLException ошибка выполнения sql
     */
    @NotNull
    @DELETE
    public Response deleteByName(@NotNull @QueryParam("name") String name) throws SQLException {
        int deletedCount = productDao.deleteByName(name);
        if (deletedCount > 0) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * Получить список товаров заданного производителя.
     *
     * @param manufacturer название производителя
     * @return http-ответ с списком товаров
     * @throws SQLException ошибка выполнения sql
     */
    @NotNull
    @GET
    @Path("/{manufacturer}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByManufacturer(@NotNull @PathParam("manufacturer") String manufacturer) throws SQLException {
        return Response.ok(productDao.getByManufacturer(manufacturer)).build();
    }
}
