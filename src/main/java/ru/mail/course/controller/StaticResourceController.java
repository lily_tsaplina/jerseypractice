package ru.mail.course.controller;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.application.Application;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URL;

/**
 * Контроллер для обработки запросов статических ресурсов.
 */
@Path("/help")
public final class StaticResourceController {
    /**
     * Получить html страницу со справочной информацией.
     *
     * @return http-ответ
     * @throws IOException ошибка вывода
     */
    @NotNull
    @GET
    @Produces("text/html; qs=0.1")
    public Response getHelpInfo() throws IOException {
        final URL resource = Application.class.getResource("/static/help-info-page.html");
        return Response.ok(resource.getContent()).build();
    }
}
