CREATE TABLE product(
  id INT NOT NULL,
  name VARCHAR(30) NOT NULL,
  manufacturer VARCHAR(30) NOT NULL,
  count int NOT NULL
);
ALTER TABLE product
ADD CONSTRAINT product_PK PRIMARY KEY (id);

CREATE SEQUENCE product_id_seq START 1;

CREATE TABLE person(
  id INT NOT NULL,
  name VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(50) NOT NULL UNIQUE
);
ALTER TABLE person
ADD CONSTRAINT person_PK PRIMARY KEY (id);

CREATE TABLE role(
  id INT NOT NULL,
  name VARCHAR(20) NOT NULL UNIQUE
);
ALTER TABLE role
ADD CONSTRAINT role_PK PRIMARY KEY (id);

CREATE TABLE person_roles(
  id INT NOT NULL,
  person_id INT NOT NULL,
  role_id INT NOT NULL
);
ALTER TABLE person_roles
ADD CONSTRAINT person_roles_PK PRIMARY KEY (id),
ADD CONSTRAINT person_roles_user_id_FK FOREIGN KEY (person_id) REFERENCES person(id),
ADD CONSTRAINT person_roles_role_id_FK FOREIGN KEY (role_id) REFERENCES role(id);